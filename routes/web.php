<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/informacion', function () {
    return view('informacion');
});

Route::get('/contacto', function () {
    return view('contacto');
});

Route::get('/software', function () {
    return view('software');
});

Route::get('/cloud', function () {
    return view('cloud');
});

Route::get('/asesoria', function () {
    return view('asesoria');
});






Auth::routes();

Route::get('/home', 'HomeController@index');
