@extends('layouts.app')

@section('content')
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-primary">Bienvenido</div>

                <div class="panel-body">
                    Gracias por ingresar a a su sistema, 
                    <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong> Hola!! <span class="fa fa-warning"></span></strong> Aqui encontraras herramientas para ti y tus clientes, mejorando y optimizando el rencimiento de tu negocio
              </div>
             <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Aplicaciones <span class="fa fa-warning"></span></strong> a tu medida, manten el control a toda hora, en todo momento y en todo lugar sin importar la distancia, genera reportes e informacion util para ti y tu empresa.
              </div>
            Optimize recursos y genere ganancias para su negocio en tiempo real
           
            </div>
          
 
             
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
