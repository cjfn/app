<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Soluciones</title>

        <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles --><!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
     <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Mis Estilos CSS-->
    <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />   


  <!-- CSS  -->
  <script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{ url('/') }}/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="{{ url('/') }}/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.6/flatly/bootstrap.css">
  <script src="{{ url('/') }}/js/Chart.js"></script>
    <script src="{{ url('/') }}/js/angular.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


</head>
<body>
    <div id="app">
      <nav class="#1565c0 blue darken-3" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo white-text"></a>
      <ul class="right hide-on-med-and-down white-text">
        <li><a href="{{ url('/') }}" class="white-text">INICIO</a></li>
       
            <!-- Dropdown Trigger -->
          <li><a class='dropdown-button btn #1565c0 blue darken-3' href='#' data-activates='dropdown1'>Nuestros Servicios</a></li>

          <!-- Dropdown Structure -->
          <ul id='dropdown1' class='dropdown-content'>
            
            <li><a href="{{ url('/software') }}"><i class="material-icons">web</i>Software</a></li>
            <li><a href="{{ url('/cloud') }}"><i class="material-icons">cloud</i>La nuve</a></li>
             <li><a href="{{ url('/asesoria') }}"><i class="material-icons">supervisor_account</i>Asesoria</a></li>
          </ul>
              
        <li><a href="{{ url('/informacion') }}" class="white-text">QUIENES SOMOS</a></li>
        <li><a href="{{ url('/contacto') }}" class="white-text">CONTACTENOS</a></li>
        <li><a href="{{ url('/login') }}">LOGIN</a></li>
        <li><a href="{{ url('/register') }}">REGISTRESE</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav ">
        <li><a href="{{ url('/') }}"    >Inicio</a></li>
            <!-- Dropdown Trigger -->
          <li><a class='dropdown-button btn #1565c0 blue darken-3' href='#' data-activates='dropdown2'>Nuestros Servicios</a></li>

          <!-- Dropdown Structure -->
          <ul id='dropdown2' class='dropdown-content'>
            
            <li><a href="{{ url('/software') }}"><i class="material-icons">web</i>Software</a></li>
            <li><a href="{{ url('/cloud') }}"><i class="material-icons">cloud</i>La nuve</a></li>
             <li><a href="{{ url('/asesoria') }}"><i class="material-icons">supervisor_account</i>Asesoria</a></li>
          </ul>
        <li><a href="{{ url('/informacion') }}">Quienes somos</a></li>
        <li><a href="{{ url('/contacto') }}">Contacto</a></li>
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
        @yield('content')
        

 

  <footer class="page-footer #1565c0 blue darken-3">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">DSoluciones</h5>
          <p class="grey-text text-lighten-4">Somos un equipo comprometido con nuestros clientes, brindamos resultados deseados, mejorando procesos y ayudandote a ser mas productivos.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Mapa del sitio</h5>
          <ul>
            <li><a class="white-text" href="#!">Nuestros servicios</a></li>
            <li><a class="white-text" href="#!">Software y aplicaciones Web</a></li>
            <li><a class="white-text" href="#!">Infraestructura</a></li>
            <li><a class="white-text" href="#!">Asesoria y ayuda</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Contacto</h5>
          <ul>
            <li><a class="white-text" href="mailto:jose@wbinnovacionreal.com">cjfn10101@gmail.com</a></li>
            <li><a class="white-text" href="tel:+50249750206">+50249750206
            </a> y <a class="white-text" href="tel:+50241390943">+50241390943
            </a></li>
           
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://dsoluciones.com.gt/">dsoluciones.com.gt</a>
      </div>
    </div>

  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
          
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV9-r12ie_d5Zu1UCV4qlTyI1ncKB29WM&callback=initMap">
    </script>


    <script>
      //incluir un html dentro de otro html
       $(function(){
      $("#includedContent").load("b.html"); 
    });

       // <iframe src="/path/to/file.html" seamless></iframe>
      //mapa
      function initMap() {
                var uluru = {lat: 14.589, lng: -90.51};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 15,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }

      //generar grafica chatjs
      /*
    var pieData = [{value: 40,color:"#0b82e7",highlight: "#0c62ab",label: "Google Chrome"},
            {
              value: 16,
              color: "#e3e860",
              highlight: "#a9ad47",
              label: "Android"
            },
            {
              value: 11,
              color: "#eb5d82",
              highlight: "#b74865",
              label: "Firefox"
            },
            {
              value: 10,
              color: "#5ae85a",
              highlight: "#42a642",
              label: "Internet Explorer"
            },
            {
              value: 8.6,
              color: "#e965db",
              highlight: "#a6429b",
              label: "Safari"
            }
          ];

          labels : ["Si otras soluciones","No, ya estamos bien como estamos","Si, un software de gestion - ERP","Si, de marketing y ventas - CRM","Si, de business intelligence","Si, de ecommerce - Tienda Online","Si, de administracion de Inventarios"],
*/
      var barChartData = {
        labels : ["SI","NO","ERP","CRM","BI","ECOMERCE","INVENTARIOS"],
        datasets : [
          {
            fillColor : "#6b9dfa",
            strokeColor : "#ffffff",
            highlightFill: "#1864f2",
            highlightStroke: "#ffffff",
            data : [30,25,10,7,5,5,2]
          },
         
        ]

      } 
        var lineChartData = {
          labels : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio"],
          datasets : [
            {
              label: "Sin software de apoyo",
              fillColor : "rgba(220,220,220,0.2)",
              strokeColor : "#6b9dfa",
              pointColor : "#1e45d7",
              pointStrokeColor : "#fff",
              pointHighlightFill : "#fff",
              pointHighlightStroke : "rgba(220,220,220,1)",
              data : [50,40,30,35,40,35,45]
            },
            {
              label: "Con software de apoyo",
              fillColor : "rgba(151,187,205,0.2)",
              strokeColor : "#e9e225",
              pointColor : "#faab12",
              pointStrokeColor : "#fff",
              pointHighlightFill : "#fff",
              pointHighlightStroke : "rgba(151,187,205,1)",
              data : [99.9,98.7,99.0,97,95,98,99]
            }
          ]

        }
    //var ctx = document.getElementById("chart-area").getContext("2d");
    //var ctx2 = document.getElementById("chart-area2").getContext("2d");
    var ctx3 = document.getElementById("chart-area3").getContext("2d");
    var ctx4 = document.getElementById("chart-area4").getContext("2d");
    //window.myPie = new Chart(ctx).Pie(pieData); 
    //window.myPie = new Chart(ctx2).Doughnut(pieData);       
    window.myPie = new Chart(ctx3).Bar(barChartData, {responsive:true});
    window.myPie = new Chart(ctx4).Line(lineChartData, {responsive:true});
    
    //generar excel
$(document).ready(function(){
    $('button').click(function(){
        var data = $('#txt').val();
        if(data == '')
            return;
        
        JSONToCSVConvertor(data, "Vehicle Report", true);
    });
});

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
</script> 
    </div>



   
</body>
</html>
