@extends('layouts.appweb')

@section('content')



  


  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center green-text">Asesoria</h1>
        <div class="row center">
          <h5 class="header col s12 green-text text-darken-2">"Asesoria para ti y tu empresa, los tiempos cambian y la revolucion tecnologica no se detiene, capacitate"</h5>
        </div>

        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="http://dsolucionesit.com/imgs/background10.jpg" alt="Unsplashed background img 1"></div>


  </div>

 
  
 <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">group_add</i></h2>
            <h5 class="center">Mejora la productividad</h5>

            <p class="light">Con el rapido surgimiento de nuevas tecnologias, las cosas han cambiado todo es para ahora, tu cliente no puede esperar a que tengas tiempo de atenderlo, haz que la capacidad de tu negocio aumente al implementar soluciones como: <br>
            <li><strong>Contabilidad e inventario: <a href="facturame/"> Facturame &copy; </a></strong> un software que te facilitara el manejo de facturas, inventarios, tomar desiciones para mantener el stock de tu empresa surtido<a href="facturame/"> Lee mas...</a></li>
            <br>
            
            <li><strong>Administra Tu Colegio: <a href="tucolegio/"> Tu Colegio &copy; </a></strong> un software que te facilitara el manejo de inscripciones, administrar tus horarios, crea clases virtuales, mensajeria, <a href="tucolegio/"> Lee mas...</a></li>
            <br>

            <li><strong>Aplicaciones Presonalizadas:<a href="personalizadas/"> Personalizame &copy; </a></strong>crea un software acorde a tus necesidades, esto hara que tus herramientas web se adapten a tus necesidades y no tu a ellas <a href="personalizadas/"> Lee mas...</a></li>
            

            </p>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">face</i></h2>
            <h5 class="center">Software para personas</h5>

            <p class="light">Pon a tu disposicion las mejores herramientas que la web puede brindarte, asesoramos en el uso de las siguientes tecnologias:
            <br>
              <li><strong>Servicios web(<a href="https://aws.amazon.com/es/">Amazon web services &copy; </a> hosting, dominios)</strong> </li>
              <br>
              <li><strong>Correos y servicios con google(<a href="https://gsuite.google.com/intl/es-419/products/gmail/?utm_source=google&utm_medium=cpc&utm_campaign=latam-CAC-all-es-dr-bkws-all-super-trial-p-latam&utm_content=text-ad-none-any-DEV_c-CRE_160663146131-ADGP_CAC%20%7C%20BKWS%20%7C%20Gmail%20%7C%20Phrase-KWID_43700015831905799&utm_term=KW_gmail-ST_gmail&gclid=CjwKEAjw3KDIBRCz0KvZlJ7k4TgSJABDqOK74DCwmM37b_50SojAyxMhPqrntytAOaFddKkrjWGSDhoC2YTw_wcB&gclsrc=aw.ds">G Suite</a>)</strong> </li>
              <br>
              <li><strong>CRM <a href="blog/"> Lee mas...</a></strong> </li>
              <br>
              <li><strong>ERP <a href="blog/"> Lee mas...</a></strong> </li>
              <br>
              <li><strong>ECOMERCE <a href="blog/"> Lee mas...</a></strong> </li>
              <br>
              <li><strong>INFRAESTRUCTURA <a href="blog/"> Lee mas...</a></strong> </li>

            </p>
          </div>
        </div>


      </div>

    </div>
  </div>
  
  

 
@endsection


        