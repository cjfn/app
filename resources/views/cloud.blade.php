@extends('layouts.appweb')

@section('content')



  


  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center blue-text">Soluciones en la nuve</h1>
        <div class="row center">
          <h5 class="header col s12 blue-text text-darken-2">Utiliza el poder de las nuevas tecnologias, no hagas mas complicado para ti y tu empresa lo que otras empresas importantes pueden darte</h5>
        </div>

        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="http://dsolucionesit.com/imgs/background6.jpg" alt="Unsplashed background img 1"></div>


  </div>

 
  
 <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">cloud_done</i></h2>
            <h5 class="center">AMAZON WEB SERVICES</h5>

            <p class="light">Amazon Web Services (AWS abreviado) es una colección de servicios de computación en la nube pública (también llamados servicios web) que en conjunto forman una plataforma de computación en la nube, ofrecidas a través de Internet por Amazon.com. Es usado en aplicaciones populares como Dropbox, Foursquare, HootSuite. Es una de las ofertas internacionales más importantes de la computación en la nube y compite directamente contra servicios como Microsoft Azure y Google Cloud Platform. Es considerado como un pionero en este campo<a href="https://es.wikipedia.org/wiki/Amazon_Web_Services"> Lee mas...</a></li>
            <br>
            
          
            </p>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">verified_user</i></h2>
            <h5 class="center">GOOGLE CLOUD</h5>

            <p class="light">  <li><strong> Google Cloud (Nube de Google), es una plataforma que ha reunido todas las aplicaciones de desarrollo web que Google estaba ofreciendo por separado. Es utilizada para crear ciertos tipos de soluciones a través de la tecnología almacenada en la nube y permite por ejemplo destacar la rapidez y la escalabilidad de su infraestructura en las aplicaciones del buscado
              <a href="https://es.wikipedia.org/wiki/Google_Cloud/"> Lee mas...</a></li>
            <br>



            </p>
          </div>
        </div>


      </div>

    </div>

            <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">cloud</i></h2>
            <h5 class="center">SERVIDORES LINUX EN LA NUVE</h5>

            <li><strong>En este tipo de computación todo lo que puede ofrecer un sistema informático se ofrece como servicio, de modo que los usuarios puedan acceder a los servicios disponibles "en la nube de Internet" sin conocimientos (o, al menos sin ser expertos) en la gestión de los recursos que usan. Según el IEEE Computer Society, es un paradigma en el que la información se almacena de manera permanente en servidores de Internet y se envía a cachés La computación en la nube son servidores desde Internet encargados de atender las peticiones en cualquier momento. Se puede tener acceso a su información o servicio, mediante una conexión a internet desde cualquier dispositivo móvil o fijo ubicado en cualquier lugar. Sirven a sus usuarios desde varios proveedores de alojamiento repartidos frecuentemente por todo el mundo. Esta medida reduce los costos, garantiza un mejor tiempo de actividad y que los sitios web sean invulnerables a los delincuentes informáticos, a los gobiernos locales y a sus redadas policiales pertenecientes. <a href="https://es.wikipedia.org/wiki/Computaci%C3%B3n_en_la_nube"> Lee mas...</a></li>
            


            </p>
          </div>
        </div>


      </div>

    </div>
  </div>
  
  

 
@endsection


        