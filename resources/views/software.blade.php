@extends('layouts.appweb')

@section('content')



  


  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br/><br/><br/><br/>
        <h1 class="header center green-text">Soluciones de software</h1>
        <div class="row center">
          <h3 class="header col s12 green-text text-darken-2">"Que tu empresa cuente con las herramientas mas potentes y confiables"</h3>
        </div>

        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="http://dsolucionesit.com/imgs/background6.jpg" alt="Unsplashed background img 1"></div>


  </div>

 
  
 <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">open_in_new</i></h2>
            <h5 class="center">Mejora la productividad</h5>

            <p class="light">Con el rapido surgimiento de nuevas tecnologias, las cosas han cambiado todo es para ahora, tu cliente no puede esperar a que tengas tiempo de atenderlo, haz que la capacidad de tu negocio aumente al implementar soluciones como: <br>
            <li><strong>Contabilidad e inventario: <a href="{{ url('/login') }}"> Facturame &copy; </a></strong> un software que te facilitara el manejo de facturas, inventarios, tomar desiciones para mantener el stock de tu empresa surtido<a href="{{ url('/login') }}"> Lee mas...</a></li>
            <br>
            
            <li><strong>Administra Tu Colegio: <a href="{{ url('/login') }}"> Tu Colegio &copy; </a></strong> un software que te facilitara el manejo de inscripciones, administrar tus horarios, crea clases virtuales, mensajeria, <a href="{{ url('/login') }}"> Lee mas...</a></li>
            <br>

            <li><strong>Aplicaciones Presonalizadas:<a href="{{ url('/login') }}"> Personalizame &copy; </a></strong>crea una herramienta acorde a tus necesidades, esto hara que tu equipo sea mas eficaz y genere una ventaja competitiva <a href="{{ url('/login') }}"> Lee mas...</a></li>
            

            </p>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">verified_user</i></h2>
            <h5 class="center">Herramientas web</h5>

            <p class="light">Pon a tu disposicion las mejores herramientas que la web puede brindarte, asesoramos en el uso de las siguientes tecnologias:
            <br>
              <li><strong>Servicios web(<a href="https://aws.amazon.com/es/">Amazon web services &copy; </a> hosting, dominios)</strong> </li>
              <br>
              <li><strong>Correos y servicios con google(<a href="https://gsuite.google.com/intl/es-419/products/gmail/?utm_source=google&utm_medium=cpc&utm_campaign=latam-CAC-all-es-dr-bkws-all-super-trial-p-latam&utm_content=text-ad-none-any-DEV_c-CRE_160663146131-ADGP_CAC%20%7C%20BKWS%20%7C%20Gmail%20%7C%20Phrase-KWID_43700015831905799&utm_term=KW_gmail-ST_gmail&gclid=CjwKEAjw3KDIBRCz0KvZlJ7k4TgSJABDqOK74DCwmM37b_50SojAyxMhPqrntytAOaFddKkrjWGSDhoC2YTw_wcB&gclsrc=aw.ds">G Suite</a>)</strong> </li>
              <br>
              <li><strong>CRM <a href="https://www.elegircrm.com/crm/que-es-un-crm"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ERP <a href="https://es.wikipedia.org/wiki/Sistema_de_planificaci%C3%B3n_de_recursos_empresariales"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ECOMERCE <a href="https://es.wikipedia.org/wiki/Comercio_electr%C3%B3nico"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>SOLUCIONES EN LA NUVE <a href="{{ url('/cloud') }}}"> Lee mas...</a></strong> </li>
                  <br>
                   <!-- generar excel -->
                      <div class='mydiv'>    
                      <textarea id="txt" class='txtarea' enabled='true'  style="display:none">[{"Vehicle":"BMW","Date":"30, Jul 2013 09:24 AM","Location":"Hauz Khas, Enclave, New Delhi, Delhi, India","Speed":42},{"Vehicle":"Honda CBR","Date":"30, Jul 2013 12:00 AM","Location":"Military Road,  West Bengal 734013,  India","Speed":0},{"Vehicle":"Supra","Date":"30, Jul 2013 07:53 AM","Location":"Sec-45, St. Angel's School, Gurgaon, Haryana, India","Speed":58},{"Vehicle":"Land Cruiser","Date":"30, Jul 2013 09:35 AM","Location":"DLF Phase I, Marble Market, Gurgaon, Haryana, India","Speed":83},{"Vehicle":"Suzuki Swift","Date":"30, Jul 2013 12:02 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Civic","Date":"30, Jul 2013 12:00 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Accord","Date":"30, Jul 2013 11:05 AM","Location":"DLF Phase IV, Super Mart 1, Gurgaon, Haryana, India","Speed":71}]</textarea>
                      <!--
                      <button class='btn btn-success'><span class='fa fa-file-excel-o'></span>Descarga informacion</button>
                    -->
                      </div>

                      <a href="{{ url('/contacto') }}" class="btn btn-success"><span class="fa fa-eye"></span>Ve mas informacion</a>

            </p>
          </div>
        </div>


      </div>

    </div>
  </div>
  
  

 
@endsection


        