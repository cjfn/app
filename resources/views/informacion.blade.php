@extends('layouts.appweb')

@section('content')



  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
       
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="http://dsolucionesit.com/imgs/background11.jpg" alt="Unsplashed background img 1"></div>


  </div>

 
  
 <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">trending_up</i></h2>
            <h5 class="center">Crea tus propias soluciones</h5>

            <p class="light">Con el rapido surgimiento de nuevas tecnologias, las cosas han cambiado todo es para ahora, tu cliente no puede esperar a que tengas tiempo de atenderlo, haz que la capacidad de tu negocio aumente al implementar soluciones como: <br>
            <li><strong>Contabilidad e inventario: <a href="{{ url('/login') }}"> Facturame &copy; </a></strong> un software que te facilitara el manejo de facturas, inventarios, tomar desiciones para mantener el stock de tu empresa surtido<a href="{{ url('/login') }}"> Lee mas...</a></li>
            <br>
            
            <li><strong>Administra Tu Colegio: <a href="{{ url('/login') }}"> Tu Colegio &copy; </a></strong> un software que te facilitara el manejo de inscripciones, administrar tus horarios, crea clases virtuales, mensajeria, <a href="{{ url('/login') }}"> Lee mas...</a></li>
            <br>

            <li><strong>Aplicaciones Presonalizadas:<a href="{{ url('/login') }}"> Personalizame &copy; </a></strong>crea una herramienta acorde a tus necesidades, esto hara que tu equipo sea mas eficaz y genere una ventaja competitiva <a href="{{ url('/login') }}"> Lee mas...</a></li>
            

            </p> 
            <center>       
             Siguenos en 
              <a href="https://bitbucket.org/cjfn/">
              <i class="fa fa-github fa-4x" aria-hidden="true"></i>
            </a>
          </center>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">verified_user</i></h2>
            <h5 class="center">Herramientas a tu medida</h5>

            <p class="light">Utilizando las mejores herramientas disponibles en el mercado en combinacion con las mejores practicas para creación de aplicaciones digitales
              <br/>
              <center>
              <i class="fa fa-desktop fa-4x" title="Aplicaciones desktop web" aria-hidden="true"></i>
               <i class="fa fa-mobile fa-4x" title="Aplicaciones responsive multiplataforma" aria-hidden="true"></i>
              <i class="fa fa-cloud fa-4x" title="Tecnologia Cloud" aria-hidden="true"></i>
              <i class="fa fa-universal-access fa-4x" title="digitalizacion de procesos" aria-hidden="true"></i>
            </center>
              <br/>
             
            <br>
              <li><strong>Servicios web(<a href="https://aws.amazon.com/es/">Amazon web services &copy; </a> hosting, dominios)</strong> </li>
              <br>
              <li><strong>Correos y servicios con google(<a href="https://gsuite.google.com/intl/es-419/products/gmail/?utm_source=google&utm_medium=cpc&utm_campaign=latam-CAC-all-es-dr-bkws-all-super-trial-p-latam&utm_content=text-ad-none-any-DEV_c-CRE_160663146131-ADGP_CAC%20%7C%20BKWS%20%7C%20Gmail%20%7C%20Phrase-KWID_43700015831905799&utm_term=KW_gmail-ST_gmail&gclid=CjwKEAjw3KDIBRCz0KvZlJ7k4TgSJABDqOK74DCwmM37b_50SojAyxMhPqrntytAOaFddKkrjWGSDhoC2YTw_wcB&gclsrc=aw.ds">G Suite</a>)</strong> </li>
              <br>
               <li><strong>CRM <a href="https://www.elegircrm.com/crm/que-es-un-crm"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ERP <a href="https://es.wikipedia.org/wiki/Sistema_de_planificaci%C3%B3n_de_recursos_empresariales"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ECOMERCE <a href="https://es.wikipedia.org/wiki/Comercio_electr%C3%B3nico"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>SOLUCIONES EN LA NUVE <a href="{{ url('/cloud') }}}"> Lee mas...</a></strong> </li>
                  <br>

            </p>
          </div>

        </div>


      </div>

    </div>
  </div>
  
  


@endsection


        