@extends('layouts.appweb')

@section('content')



  <div class="carousel carousel-slider center" data-indicators="true">
    <div class="carousel-fixed-item center">
    
    </div>
    <div class="carousel-item white-text" href="#one!">
   
       <img src="http://web.wbrealinnovation.com/imgs/BG1.jpg">
    </div>
    <div class="carousel-item white-text" href="#two!">
      
      <img src="http://web.wbrealinnovation.com/imgs/BG2.jpg">
    </div>
    <div class="carousel-item white-text" href="#three!">
     
       <img src="http://web.wbrealinnovation.com/imgs/BG3.jpg">
    </div>

  </div>

  <div class="container">

        
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">dashboard</i></h2>
            <h5 class="center">Soluciona problemas de tu empresa</h5>

            <p class="light">No dejes que tu y tu empresa cometan dos veces el mismo error, por que tus clientes te eligen por tus productos, no los pierdas por un mal servicio, <strong> Piensa y reacciona rapido,</strong> utilizando las mejores herramientas disponibles.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
             <h2 class="center brown-text"><i class="material-icons">trending_up</i></h2>
            <h5 class="center"><STRONG>Mejora la productividad</STRONG></h5>
            <center>
              <STRONG style="color:yellow;" title="ERP, CRM, BI, ECOMERCE, ETC">Con Software de apoyo</STRONG> 
                <canvas id="chart-area4" width="300" height="150"></canvas>

              <STRONG style="color:blue">Sin Software de apoyo</STRONG> 
            </center>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">language</i></h2>
            <h5 class="center">Globalizate</h5>

            <p class="light">Abre las puertas de tu negocio no solo en tu localidad, si no a <strong>todo el mundo,</strong>  por que no puedes conformarte con lo que tienes cuando puedes tener mas, el mundo esta cambiando a pasos agigantados, tu competencia ya lo sabe</p>
          </div>
        </div>
      </div>

    </div>
  </div>



  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center light-text">Soluciones a tu medida</h1>
        <div class="row center">
          <h5 class="header col s12 light-text text-darken-2">"Por que no puedes esperar resultados diferentes haciendo siempre lo mismo"</h5>
        </div>
        <div class="row center">
          <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light #1565c0 blue darken-3">Conoce nuestros servicios</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="http://web.wbrealinnovation.com/imgs/background2.jpg" alt="Unsplashed background img 1"></div>


  </div>

  
 <div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            
              <center>
                <h3><strong>
                  Empresas que estan dispuestas a contratar una nueva solucion  de software en Guatemala 2018 
                </strong>
                </h3>
                <div id="canvas-holder">
                <canvas id="chart-area3" width="600" height="500"></canvas>
                </div>
              </center>
      <center>
     
      </center>
      </div>
      </div>
            <div class="col s12 m6">
              <div class="icon-block">
                <h2 class="center brown-text"><i class="material-icons">verified_user</i></h2>
                <h5 class="center">Herramientas web</h5>

                <p class="light">Pon a tu disposicion las mejores herramientas que la web puede brindarte, asesoramos en el uso de las siguientes tecnologias:
                <br>
                  <li><strong>Servicios web(<a href="https://aws.amazon.com/es/">Amazon web services &copy; </a> hosting, dominios)</strong> </li>
                  <br>
                  <li><strong>Correos y servicios con google(<a href="https://gsuite.google.com/intl/es-419/products/gmail/?utm_source=google&utm_medium=cpc&utm_campaign=latam-CAC-all-es-dr-bkws-all-super-trial-p-latam&utm_content=text-ad-none-any-DEV_c-CRE_160663146131-ADGP_CAC%20%7C%20BKWS%20%7C%20Gmail%20%7C%20Phrase-KWID_43700015831905799&utm_term=KW_gmail-ST_gmail&gclid=CjwKEAjw3KDIBRCz0KvZlJ7k4TgSJABDqOK74DCwmM37b_50SojAyxMhPqrntytAOaFddKkrjWGSDhoC2YTw_wcB&gclsrc=aw.ds">G Suite</a>)</strong> </li>
                  <br>
                  <li><strong>CRM <a href="https://www.elegircrm.com/crm/que-es-un-crm"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ERP <a href="https://es.wikipedia.org/wiki/Sistema_de_planificaci%C3%B3n_de_recursos_empresariales"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>ECOMERCE <a href="https://es.wikipedia.org/wiki/Comercio_electr%C3%B3nico"> Lee mas...</a></strong> </li>
                  <br>
                  <li><strong>SOLUCIONES EN LA NUVE <a href="{{ url('/cloud') }}}"> Lee mas...</a></strong> </li>
                  <br>
                   <!-- generar excel -->
                      <div class='mydiv'>    
                      <textarea id="txt" class='txtarea' enabled='true'  style="display:none">[{"Vehicle":"BMW","Date":"30, Jul 2013 09:24 AM","Location":"Hauz Khas, Enclave, New Delhi, Delhi, India","Speed":42},{"Vehicle":"Honda CBR","Date":"30, Jul 2013 12:00 AM","Location":"Military Road,  West Bengal 734013,  India","Speed":0},{"Vehicle":"Supra","Date":"30, Jul 2013 07:53 AM","Location":"Sec-45, St. Angel's School, Gurgaon, Haryana, India","Speed":58},{"Vehicle":"Land Cruiser","Date":"30, Jul 2013 09:35 AM","Location":"DLF Phase I, Marble Market, Gurgaon, Haryana, India","Speed":83},{"Vehicle":"Suzuki Swift","Date":"30, Jul 2013 12:02 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Civic","Date":"30, Jul 2013 12:00 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Accord","Date":"30, Jul 2013 11:05 AM","Location":"DLF Phase IV, Super Mart 1, Gurgaon, Haryana, India","Speed":71}]</textarea>
                      <!--
                      <button class='btn btn-success'><span class='fa fa-file-excel-o'></span>Descarga informacion</button>
                    -->
                      </div>

                      <a href="{{ url('/contacto') }}" class="btn btn-success"><span class="fa fa-eye"></span>Ve mas informacion</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  

 
  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          
          <h3 class="header col s12 green-text"><strong>Una solucion integral para ti y tus clientes</strong></h3>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="http://web.wbrealinnovation.com/imgs/background1.jpg" alt="Unsplashed background img 2"></div>
  </div>

  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">dashboard</i></h2>
            <h5 class="center">Crea aplicaciones personalizadas</h5>
  
            <p class="light">No dejes que tu y tu empresa cometan dos veces el mismo error, por que tus clientes te eligen por tus productos, no los pierdas por un mal servicio, <strong> Piensa y reacciona rapido,</strong> utilizando las mejores herramientas disponibles.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">group</i></h2>
            <h5 class="center">Enfocado en las personas</h5>

            <p class="light">Todas las herramientas que creamos estan enfocados al <strong>usuario final</strong>, por ello estan diseñadas de manera facil e intuitiva, para que disfrutes de tareas que antes eran tediosas y repetitivas.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
          <h2 class="center brown-text"><i class="material-icons">group</i></h2>
            <h5 class="center">Enfocado en las personas</h5>

            <p class="light">Todas las herramientas que creamos estan enfocados al <strong>usuario final</strong>, por ello estan diseñadas de manera facil e intuitiva, para que disfrutes de tareas que antes eran tediosas y repetitivas.</p>





          </div>
        </div>
      </div>

    </div>
  </div>

 <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h3 class="header col s12 light-text"><strong>"Toda mejora involucra no solo al Gerente si no a todos sus colaboradores y sus clientes"</strong></h3>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="http://web.wbrealinnovation.com/imgs/BG4.jpg" alt="Unsplashed background img 3"></div>
  </div>


  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>

          <div class="col s12 m5">
          <h4>Contactanos</h4>

          <p class="left-align light">Estamos ubicados en Zona 10 12 calle 11-65, Ciudad de Guatemala, telefono <a href="tel:+50249750206">+50249750206</a></p>
            <div class="row">
              Registrate y nos estaremos comunicando  lo mas rapido posible
               <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12 control-label">Nombre de usuario</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">E-Mail </label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Contraseña/Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-12 control-label">Confirma password</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-danger">
                                    Registrate 
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
                  

             

          </div>

          <div class="col s12 m7">
          <br>
          <br>
          <br>
          <br>
          <br>
          <div id="map"></div>
        </div>
        </div>
      </div>

    </div>
  </div>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-69265836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-69265836-1');
</script>


@endsection


        